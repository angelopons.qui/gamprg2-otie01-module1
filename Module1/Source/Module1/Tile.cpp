// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"

#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"

#include "RunGameMode.generated.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("Scene Component");
	SetRootComponent(SceneComponent);

	ArrowComponent = CreateDefaultSubobject<UArrowComponent>("Arrow Component");
	ArrowComponent->SetupAttachment(RootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>("Box Component");
	BoxComponent->SetupAttachment(RootComponent);

	ObstacleSpawnArea = CreateDefaultSubobject<UBoxComponent>("Obstacle Spawn Area");
	ObstacleSpawnArea->SetupAttachment(RootComponent);

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnHit);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();	

	int rand = FMath::RandRange(0, 1);
	
	if (rand == 0) {
		SpawnObstacle();
	}
	else {
		SpawnPickup();
	}

}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::OnHit(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* runCharacter = Cast<ARunCharacter>(OtherActor))
	{
		OnTileCollided.Broadcast(this);
	}
}

void ATile::SpawnObstacle()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FTransform transform;
	transform.SetLocation(UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnArea->GetComponentLocation(), ObstacleSpawnArea->GetScaledBoxExtent()));

	AObstacle* newObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleClass[FMath::RandRange(0, ObstacleClass.Num() - 1)], transform, spawnParams);
	OnDestroyed.AddDynamic(newObstacle, &AObstacle::DestroyOnCalled);
	newObstacle->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true), ATile::GetAttachParentSocketName());
}

void ATile::SpawnPickup()
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FTransform transform;
	transform.SetLocation(UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnArea->GetComponentLocation(), ObstacleSpawnArea->GetScaledBoxExtent()));

	APickup* newPickup = GetWorld()->SpawnActor<APickup>(PickupClass[FMath::RandRange(0, PickupClass.Num() - 1)], transform, spawnParams);
	OnDestroyed.AddDynamic(newPickup, &APickup::DestroyOnCalled);
	newPickup->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepWorld, true), ATile::GetAttachParentSocketName());
}