// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"

#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("Spring Arm");
	SpringArm->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);
	Camera->SetRelativeLocation(FVector(-500.0f, 0, 0));

	//OnActorHit.AddDynamic(this, &ARunCharacter::OnHit);
}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ARunCharacter::OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
}

void ARunCharacter::Die()
{
	if (isDead) {
		return;
	}

	isDead = true;

	this->GetWorld()->GetFirstPlayerController()->UnPossess();
	GetMesh()->SetVisibility(false);

	OnActorDeath.Broadcast(this);
}

void ARunCharacter::AddCoin()
{
	TotalCoins += 1;
}
