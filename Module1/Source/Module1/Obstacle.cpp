// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"

#include "Components/StaticMeshComponent.h"

#include "RunCharacter.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");
	StaticMeshComponent->SetupAttachment(RootComponent);
	StaticMeshComponent->OnComponentHit.AddDynamic(this, &AObstacle::OnHit);
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AObstacle::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (ARunCharacter* runCharacter = Cast<ARunCharacter>(OtherActor))
	{
		OnTrigger(runCharacter);
	}
}

void AObstacle::DestroyOnCalled(AActor* actor)
{
	Destroy();
}
