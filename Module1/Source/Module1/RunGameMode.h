// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

UCLASS()
class MODULE1_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARunGameMode();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ARunCharacter* Player;

	UPROPERTY(EditAnywhere)
	class TSubclassOf<class ATile> TileClass;

protected:
	//
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector LastTileLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void SpawnTile(class ATile* tile);

	UFUNCTION()
	void DestroyTile(class ATile* tile);
};
