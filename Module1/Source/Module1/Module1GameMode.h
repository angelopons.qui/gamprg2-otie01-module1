// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Module1GameMode.generated.h"

UCLASS(minimalapi)
class AModule1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AModule1GameMode();
};



