// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"

#include "TimerManager.h"
#include "Delegates/DelegateSignatureImpl.inl"

#include "RunCharacter.h"
#include "Tile.h"

ARunGameMode::ARunGameMode()
{
	
}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	Player = Cast<ARunCharacter>(GetWorld()->GetFirstPlayerController()->GetPawn());

	//Initialize first group of tiles
	for (int i = 0; i < 8; i++) {
		SpawnTile(NULL);
	}
}

void ARunGameMode::SpawnTile(class ATile* tile) 
{
	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	FTransform transform;
	transform.SetLocation(LastTileLocation);

	//Spawn new tile and bind listener.
	ATile* newTile = GetWorld()->SpawnActor<ATile>(TileClass, transform, spawnParams);
	newTile->OnTileCollided.AddDynamic(this, &ARunGameMode::SpawnTile);
	newTile->OnTileCollided.AddDynamic(this, &ARunGameMode::DestroyTile);

	//Increment next position
	LastTileLocation.X += 1000;
}

void ARunGameMode::DestroyTile(class ATile* tile)
{
	FTimerHandle DestroyTimer;
	FTimerDelegate TimerDelegate;

	TimerDelegate.BindLambda([tile] {tile->Destroy();});
	GetWorldTimerManager().SetTimer(DestroyTimer, TimerDelegate, 1.0f, false);

}