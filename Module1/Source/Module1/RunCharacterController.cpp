// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"

#include "RunCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ARunCharacterController::ARunCharacterController()
{
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	RunCharacter = Cast<ARunCharacter, APawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	Possess(RunCharacter);
}

void ARunCharacterController::MoveForward(float scale)
{
	RunCharacter->AddMovementInput(RunCharacter->GetActorForwardVector(), scale);
}


void ARunCharacterController::MoveRight(float scale)
{
	RunCharacter->AddMovementInput(RunCharacter->GetActorRightVector(), scale);
}

// Called every frame
void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	MoveForward(1.0f);

}

// Called to bind functionality to input
void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}
