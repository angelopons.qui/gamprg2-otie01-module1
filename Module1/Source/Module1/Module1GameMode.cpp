// Copyright Epic Games, Inc. All Rights Reserved.

#include "Module1GameMode.h"
#include "Module1Character.h"
#include "UObject/ConstructorHelpers.h"

AModule1GameMode::AModule1GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
